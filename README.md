# myapp

generated using Luminus version "2.9.11.16"

FIXME

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.
And You will need mongo db installed.
[1]: https://github.com/technomancy/leiningen

### Installing

* Install [leiningen](http://leiningen.org/)
* Install [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community)
* Create a new MongoDB database
* Create a new user for the new database
* Edit `src/clj/dao/db_conf.clj` with the new db and credentials
* `lein run`

## License

Copyright © 2016 FIXME
