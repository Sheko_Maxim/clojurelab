(ns myapp.utils.resource
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [myapp.db.user :as user-dao])
  (:import (java.util Date)))

(defn create-cookie
  [cookie]
  (str "user=" cookie "; expires=" (.toString (Date. (+ (.getTime (Date.)) (* 1000 60 60 24 365 10)))) "; path=/; HttpOnly"))

;;TODO check secure flag
(defn delete-cookie
  [cookie]
  (str "user=" cookie "; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/; HttpOnly; secure"))

(defn convert-data-map
  "Converts clojure map's string keywords to keyword functions"
  [json-clojure-map]
  (reduce #(assoc %1 (keyword (first %2)) (second %2)) {} json-clojure-map))

(defn get-cookie
  [ctx]
  (-> ctx :request :cookies (get "user") :value))

(defn get-username-from-cookie
  [ctx]
  (if-let [cookie (get-cookie ctx)]
    (.substring cookie 0 (str/index-of cookie "&"))))

(defn get-username
  [ctx]
  (-> ctx :user-obj :username))

(defn capitalize
  [text]
  (let [s (str/split text #"\s+")
        first-s (first s)
        rest-s (rest s)]
    (apply str (interpose " " (concat [(str/capitalize first-s)] rest-s)))))
