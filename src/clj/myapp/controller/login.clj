(ns myapp.controller.login
  (:require [liberator.core :refer [resource defresource]]
            [liberator.representation :as rep]
            [myapp.utils.resource :as resource-util]
            [myapp.db.user :as user-dao]))

(defn sign-up
  []
  (fn [req]
    (let [password (get-in req [:params :password])
          username (get-in req [:params :username])]
      (user-dao/create-user username password)
      (str "Do something with " username ":" password)))
)
