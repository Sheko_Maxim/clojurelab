(ns myapp.controller.entry
  (:require [liberator.core :refer [resource defresource]]
            [hiccup.core :as hiccup]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [myapp.layout :as layout]
            [myapp.db.entry :as entry-dao]
            [monger.json])
  (:import (java.util Calendar Date)))

(defn home-page
  []
  (layout/render
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(fn [req]
  (let [password (get-in req [:params :password])
        username (get-in req [:params :username])]
    (user-dao/create-user username password)
    (str "Do something with " username ":" password)))

(defn create-story
  []
  (fn [req]
    (let [title (get-in req [:params :title])
          text (get-in req [:params :text])
          ganre (get-in req [:params :ganre])]
      (entry-dao/create-story title text ganre)
      (str "Story sucsessful " title ":" text)))
  )
