(ns myapp.routes.home
  (:require [myapp.layout :as layout]
            [compojure.core :refer [defroutes GET PUT]]
            [ring.util.http-response :as response]
            [ring.util.request :refer [body-string content-type]]
            [clojure.java.io :as io]
            [myapp.controller.login :as controller]
;            [myapp.controller.entry :as homecontroller]
            ))

(defn home-page []
  (layout/render
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page []
  (layout/render "about.html"))

(defroutes home-routes
  (GET "/" [] (home-page))
  (PUT "/put" [] (fn [req]
                     (let [title (get-in req [:params :title])
                           author (get-in req [:params :author])]
                       (str "Do something with" title author))))
  (PUT "/signup" [](controller/sign-up))
;  (PUT "/story" [](homecontroller/add-story))
  (GET "/about" [] (about-page)))

